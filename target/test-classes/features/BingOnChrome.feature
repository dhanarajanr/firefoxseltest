@bing @chrome

Feature: Do a Bing search in Chrome browser

  Scenario: Search a simple text in chrome browser
    Given the firefox browser is open
    And the browser opens "https://www.bing.com/"
    When I search for "Selenium" text
    Then I should see "https://www.selenium.dev" in the search results
