package stepDefns;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;
import java.util.List;


public class FirefoxStepdefs {
    WebDriver webDriver;
    public String screenshot_file_path = "demoScreenshot.png";
    public String rp_message = "test message for Report Portal";

    @Before(order = 1)
    public void setUp1() {
        System.out.println("Inside Setup 1");
    }

    @Before(order = 3)
    public void setUp2() {
        System.out.println("Inside Setup 2");
    }

    @Before(value = "@chrome", order = 3)
    public void setUp3() {
        System.out.println("Inside Setup 3");
    }

    @Given("the firefox browser is open")
    public void theFirefoxBrowserIsOpen() /*throws MalformedURLException*/ {
        WebDriverManager.firefoxdriver().setup();
        webDriver = new FirefoxDriver();
    }

    @And("the browser opens {string}")
    public void theBrowserOpens(String arg0) {
        webDriver.get(arg0);
    }

    @When("I search for {string} text")
    public void iSearchForText(String arg0) {
        webDriver.findElement(By.name("q")).sendKeys(arg0);
        webDriver.findElement(By.name("search")).submit();
    }

    @Then("I should see {string} in the search results")
    public void iShouldSeeInTheSearchResults(String arg0) {
        List<WebElement> searchResults = webDriver.findElements(By.cssSelector("div ol#b_results>li cite"));
        boolean resultFound = false;
        for (WebElement eachResult : searchResults) {
            if (eachResult.getText().equalsIgnoreCase(arg0)) {
                resultFound = true;
                break;
            }
        }
        Assert.assertTrue("Search Results does not contain " + arg0, resultFound);
    }

    @After
    public void tearDown(Scenario scenario) throws IOException {
        if (webDriver != null) {
            scenario.log("==> Page URL: " + webDriver.getCurrentUrl());
            byte[] screenshotBytes = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshotBytes, "image/png", "==> Scenario: " + scenario.getName());
            webDriver.quit();
        }
    }

    @AfterStep
    public void takeScreenshot(Scenario scenario) /*throws IOException */ {
        if (webDriver != null) {
            scenario.log("==> Page URL: " + webDriver.getCurrentUrl());
            byte[] screenshotBytes = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshotBytes, "image/png", "==> Scenario: " + scenario.getName());
        }
    }
}
