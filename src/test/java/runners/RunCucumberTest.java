package runners;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/reports/cucumber-html-report-testng.html",
                "junit:target/reports/junit-report-testng.xml",
                "json:target/reports/cucumber-testng.json",
                "pretty"},
        features = {"src/test/resources/features"},
        tags = "@bing",
        glue = {"stepDefns"}
)
public class RunCucumberTest {
}
