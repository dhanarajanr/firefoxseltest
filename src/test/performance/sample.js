import { sleep } from "k6";
import http from "k6/http";

export const options = {
  stages: [
    { duration: "12s", target: 20 },
    { duration: "1m36s", target: 20 },
    { duration: "12s", target: 0 },
  ],
  ext: {
    loadimpact: {
      distribution: {
        "amazon:gb:london": { loadZone: "amazon:gb:london", percent: 100 },
      },
    },
  },
};

export default function main() {
  let response;

  response = http.get("http://test.k6.io");

  // Automatically added sleep
  sleep(1);
}
